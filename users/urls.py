from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^login/',
        views.LoginView.as_view(),
        name='login'),

    url(r'^password-change/',
        views.change_password,
        name='password_change'),

    url(r'^logout/$',
        views.LogoutView.as_view(),
        name='logout'),

    url(r'^sign-up/$',
        views.SignUpView.as_view(),
        name='sign_up'),

]
