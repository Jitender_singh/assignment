(function() {

    $(document).ready(function () {


        var initial_date_ele = "#initialDateTime",
            ending_date_ele = "#endingDateTime";

        $(initial_date_ele).datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false //Important! See issue #1075
        });
        $(ending_date_ele).datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false //Important! See issue #1075

        });


        $(initial_date_ele).on("dp.change", function (e) {
            // set min date of the end_date
            $(ending_date_ele).data("DateTimePicker").minDate(e.date);
        });

        $(ending_date_ele).on("dp.change", function (e) {
            // set the max date of the start_date.
            $(initial_date_ele).data("DateTimePicker").maxDate(e.date);

        });


        $("#object_detail").click(function (e) {
            var intialDateTime = $(initial_date_ele).data("DateTimePicker").date().format("YYYY-MM-DD HH:mm:ss"),
                endingDateTime = $(ending_date_ele).data("DateTimePicker").date().format("YYYY-MM-DD HH:mm:ss"),
                selected_object = $('#selected_object_id').val();


            if ((typeof intialDateTime !== "undefined" && intialDateTime !== null)
                && (typeof endingDateTime !== "undefined" && endingDateTime !== null)
                && (typeof selected_object !== "undefined" && selected_object !== null)){

                var url = $(this).attr('data-redirect');
                window.location = url+"?start="+intialDateTime+"&end="+endingDateTime+"&obj="+selected_object
            }else{
                alert("Please check you have selected correct information");
            }

        });

    });

})();