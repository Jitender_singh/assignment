from django.db import models
from django.contrib.auth.models import User
from aldjemy.meta import AldjemyMeta
from django.conf import settings


class Object(models.Model):
    nid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, null=True, blank=True)
    valid_from = models.DateTimeField(null=True)
    valid_to = models.DateTimeField(null=True)
    created = models.DateTimeField(auto_now_add=True, null=True)
    nofqueries = models.IntegerField(default=0)
    nofsqueries = models.IntegerField(default=0)
    lastquery = models.DateTimeField(null=True)

    def __unicode__(self):
        return str(self.nid)

    __metaclass__ = AldjemyMeta


class Location(models.Model):
    nid = models.AutoField(primary_key=True)
    ts = models.DateTimeField(null=True)
    object = models.ForeignKey(Object, related_name="loc_object")
    bssid = models.CharField(max_length=50, null=True, blank=True)
    longitude = models.FloatField(default=0)
    latitude = models.FloatField(default=0)
    azimuth = models.FloatField(default=0)
    distance = models.FloatField(default=0)
    other = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return str(self.nid)

    __metaclass__ = AldjemyMeta

    @property
    def get_the_location(self):
        # return dict
        return {'location': [float(self.longitude), float(self.latitude)],
                'other': self.other,
                'time': str(self.ts.strftime("%a, %d %b %Y %H:%M:%S"))}

    @classmethod
    def get_data(cls, object_id):
        # return the all data with respect to object.
        return cls.sa.query() \
            .filter_by(object_id=object_id) \
            .order_by(cls.sa.nid.desc()) \
            .limit(settings.MAX_POINTS).all()

    @classmethod
    def get_report(cls, object_id, start_date, end_date, with_point_limit=False):
        # Return the all location data for report generation
        report = cls.sa.query().filter(cls.sa.ts.between(start_date, end_date))
        if object_id:
            report = report.filter_by(object_id=int(object_id))
        if with_point_limit:
            report = report.order_by(cls.sa.nid.desc()) \
                .limit(settings.MAX_POINTS)
        return map(lambda reports: reports.get_the_location, report.all())


class ObjectUser(models.Model):
    user = models.ForeignKey(User, related_name="user")
    object = models.ForeignKey(Object, related_name="object")
    visible = models.BooleanField(default=True)

    def __unicode__(self):
        return str(self.visible)

    __metaclass__ = AldjemyMeta

    @classmethod
    def get_users_object(cls, user_id):
        # return user objects
        return cls.sa.query().filter_by(user_id=user_id).filter_by(visible=True).limit(settings.MAX_OBJECTS).all()

    @classmethod
    def get_all_visible_objects(cls, user_id):
        # return all the visible objects
        object_list = cls.get_users_object(user_id)
        if object_list:
            return cls.get_objects_coordinates(object_list)
        return None

    @classmethod
    def get_objects_coordinates(cls, objects):
        # return the coordinates with respect to object.
        if objects:
            return map(lambda obj: {
                'name': obj.object.name,
                'data': map(lambda loc: loc.get_the_location, Location.get_data(obj.object_id))}, objects)
        return None


class MapSetting(models.Model):
    max_point = models.IntegerField(default=0)
    max_object = models.IntegerField(default=0)
    modified = models.DateTimeField(auto_now=True)

    @classmethod
    def set_settings(cls):
        # set the settings
        map_obj = cls.sa.query().order_by(cls.sa.modified.desc()).first()
        if map_obj:
            settings.MAX_OBJECTS = map_obj.max_object
            settings.MAX_POINTS = map_obj.max_point

    def __unicode__(self):
        return str(self.max_point)

    __metaclass__ = AldjemyMeta
