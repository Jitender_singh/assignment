(function() {

    // vectorLine source
    var vectorLine = new ol.source.Vector({});


    // Map
    var map = new ol.Map({
        layers: [
            new ol.layer.Tile({
                source: new ol.source.OSM(),
            })
        ],
        target: 'map',
        controls: ol.control.defaults({
            attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
                collapsible: false
            })
        }),
        view: new ol.View({
            center: [0, 0],
            zoom: 3,
            minZoom: 3,
            maxZoom: 13
        })
    });
    // Method remove Overlays
    function removeAllOverlay(map) {
        map.getOverlays().clear();

    }

    // Add overlay in map
    function addoverlay(map, data) {
        var point = ol.proj.transform(
            data.location,
            'EPSG:4326',
            'EPSG:3857'
        );
        map.addOverlay(new ol.Overlay({
            position: point,
            element: $('<img class="location-popover" src="/static/img/pin-blue.png" hieght="10px" width="16px" data-trigger="hover">')
                .css({marginTop: '-200%', marginLeft: '-50%', cursor: 'pointer'})
                .popover({
                    'placement': 'top',
                    'html': true,
                    'trigger': 'manual',
                    'content': '<span> Location:- ' + data.other + '<br>Time:- <i>' + data.time + '</i></span>'
                })
                .on("mouseenter", function () {
                    $(this).popover('show');
                    $(".location-popover").not(this).popover('hide');
                })
                .on("mouseleave", function () {
                    $(".location-popover").popover('hide');
                })

        }));
        return point;
    }


    // join the point
    function joinPoints(map, points) {
        var color = getRandomColor(),
            featureLine = new ol.Feature({
                geometry: new ol.geom.LineString(points)
            });
        // add feature
        vectorLine.addFeature(featureLine);

        // vector lines
        var vectorLineLayer = new ol.layer.Vector({
            source: vectorLine,
            style: new ol.style.Style({
                fill: new ol.style.Fill({color: color, weight: 4}),
                stroke: new ol.style.Stroke({color: color, width: 2})
            })
        });

        map.addLayer(vectorLineLayer);
    }


    function getRandomColor() {
        // Return the random color;
        var letters = '0123456789ABCDEF'.split(''),
            color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

    $(document).ready(function () {
        var start_date=$("#start_date").val().trim(),
            end_date=$("#end_date").val().trim(),
            vehicle=$("#vehicle").val().trim();
        if (start_date != "" && end_date !="" && vehicle !=""){
            getObjectLocationData(start_date, end_date, vehicle);
        }else{
            getAllData();
        }

    function getAllData(){
        // return the object(vehicle)
        // location in formation.
        simpleAjaxCall(null, "/dashboard/get_location_data/").done(function (response, status, jqXHR) {
                for (index in response) {
                    var data = response[index].data;
                    var points = [];
                    for (i in data) {
                        points.push(addoverlay(map, data[i]));
                    }
                    joinPoints(map, points);
                }
            }).fail(function (jqXHR, status, error) {
                alert("Internal Server Error!!!")
            });
        }


        function getObjectLocationData(start_date, end_date, object_id) {
            // return the object(vehicle)
            // location in formation.

            var data = {'start_date': start_date,
                'end_date': end_date,
                'object_id': object_id,
                'point_limit': true};

            simpleAjaxCall(null, "/dashboard/get_report/", null, data).done(function (response, status, jqXHR) {
                vectorLine.clear(); // clear the lines
                removeAllOverlay(map); // clear the overlay
                var points = [];
                for (index in response) {
                    points.push(addoverlay(map, response[index]));
                }
                joinPoints(map, points);// draw thw lines and overlay

            }).fail(function (jqXHR, status, error) {
                alert("Internal Server Error!!!")
            });
        }

    });

})();