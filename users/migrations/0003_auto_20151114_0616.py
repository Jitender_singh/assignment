# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20151114_0545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='azimuth',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='location',
            name='distance',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='location',
            name='latitude',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='location',
            name='longitude',
            field=models.FloatField(default=0),
        ),
    ]
