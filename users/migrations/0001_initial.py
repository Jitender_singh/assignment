# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('nid', models.AutoField(serialize=False, primary_key=True)),
                ('ts', models.DateTimeField(auto_now_add=True, null=True)),
                ('bssid', models.CharField(max_length=50, null=True, blank=True)),
                ('longitude', models.BigIntegerField(default=0)),
                ('latitude', models.BigIntegerField(default=0)),
                ('azimuth', models.BigIntegerField(default=0)),
                ('distance', models.BigIntegerField(default=0)),
                ('other', models.TextField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Object',
            fields=[
                ('nid', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=50, null=True, blank=True)),
                ('valid_from', models.DateTimeField(null=True)),
                ('valid_to', models.DateTimeField(null=True)),
                ('created', models.DateTimeField(auto_now_add=True, null=True)),
                ('nofqueries', models.IntegerField(default=0)),
                ('nofsqueries', models.IntegerField(default=0)),
                ('lastquery', models.DateTimeField(null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ObjectUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('visible', models.BooleanField(default=True)),
                ('object', models.ForeignKey(related_name='object', to='users.Object')),
                ('user', models.ForeignKey(related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='location',
            name='object',
            field=models.OneToOneField(related_name='loc_object', to='users.Object'),
        ),
    ]
