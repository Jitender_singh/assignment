from django.contrib import admin
from django.conf import settings
from .models import Location, Object, ObjectUser, MapSetting


class LocationAdmin(admin.ModelAdmin):
    list_display = ('other', 'longitude', 'latitude', 'ts', 'object')


class LocationInline(admin.StackedInline):
    model = Location


class ObjectAdmin(admin.ModelAdmin):
    inlines = (LocationInline,)
    list_display = ('name', 'valid_from', 'valid_to', 'nofqueries', 'nofsqueries')


class ObjectUserAdmin(admin.ModelAdmin):
    list_display = ('user', 'object', 'visible')


class MapSettingAdmin(admin.ModelAdmin):
    list_display = ('max_point', 'max_object')

    def save_model(self, request, obj, form, change):
        # override the save_model method
        super(MapSettingAdmin, self).save_model(request, obj, form, change)
        settings.MAX_POINTS = obj.max_point,
        settings.MAX_OBJECTS = obj.max_object

admin.site.register(Object, ObjectAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(ObjectUser, ObjectUserAdmin)
admin.site.register(MapSetting, MapSettingAdmin)
