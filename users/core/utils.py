"""
Some Common function.
"""
from django.contrib.auth.models import User
from django.contrib import messages
import datetime


def get_user_obj_through_email(email):
    """
    Return the user object.
    """
    user_obj = User.objects.filter(email__iexact=email)
    if user_obj:
        return user_obj[0]
    return False


def set_messages(request=None, msg_text=None, msg_level=None):
    """
    Set the session messages.
    If the message is set then it return `True`, otherwise return `False`
    """
    if request and msg_text and msg_level:
        messages.add_message(request=request,
                             level=msg_level,
                             message=msg_text)
        return True
    return False


def convert_to_date(date_str, str_format='%Y-%m-%d %H:%M:%S'):
    # Convert the date string to datetime object.
    try:
        return datetime.datetime.strptime(date_str, str_format)
    except ValueError:
        return None