(function(){

    function get_report(start_date, end_date, object_id) {

        // Report generation function

        var data = {'start_date': start_date, 'end_date': end_date, 'object_id': object_id};

        // Ajax call
        simpleAjaxCall(null, "/dashboard/get_report/", null, data).done(function (response, status, jqXHR) {
            var template = '<div class="panel panel-default">' +
                '<div class="panel-heading">Report</div>' +
                '<table class="table table-striped">' +
                '<tr><th>Lat,Long</th><th>Info</th><th>Time</th></tr>';
            var data = '';
            if (response.length > 0) {

                for (i in response) {
                    data += '<tr><td>' + response[i].location[1] + ',' + response[i].location[0] + '</td><td>' + response[i].other + '</td><td>' + response[i].time + '</td></tr>';
                }
            } else {
                data = "<tr><td colspan=3>No Record Found </td></tr>"
            }
            template += data + '</table></div>';

            $("#reports").html(template); // generate the report

        }).fail(function (jqXHR, status, error) {
            alert("Internal Server Error!!!")
        });
    }



        $(document).ready(function () {

        var start_date_ele = '#datetimepicker6',
            end_date_ele = '#datetimepicker7',
            select_ele = "#object_id";

        $(start_date_ele).datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false //Important! See issue #1075
        });
        $(end_date_ele).datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false //Important! See issue #1075

        });

        $(start_date_ele).on("dp.change", function (e) {
            // set min date of the end_date
            $(end_date_ele).data("DateTimePicker").minDate(e.date);
        });

        $(end_date_ele).on("dp.change", function (e) {
            // set the max date of the start_date.
            $(start_date_ele).data("DateTimePicker").maxDate(e.date);

        });

        $("#report_generate").click(function(){
            var start_date = $(start_date_ele).data("DateTimePicker").date().format("YYYY-MM-DD HH:mm:ss"),
                end_date = $(end_date_ele).data("DateTimePicker").date().format("YYYY-MM-DD HH:mm:ss"),
                selected_object = $(select_ele).val();
            get_report(start_date, end_date, selected_object);
        });

    });










})();