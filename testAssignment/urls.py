from django.conf.urls import include, url
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.views.generic import RedirectView

urlpatterns = [
    # Examples:
    # url(r'^$', 'testAssignment.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('users.urls', namespace='users')),
    url(r'^dashboard/', include('dashboard.urls', namespace='dashboard')),
    url(r'^.*$',
        RedirectView.as_view(url=reverse_lazy('users:login'), permanent=False),
        name='index')
]
