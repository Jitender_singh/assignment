MSG_USER_LOGIN_SUCCESS = "Welcome! You are successfully log-in"
MSG_USER_LOGOUT_SUCCESS = "Thanks for using our application!" \
                          " You are successfully logout."
MSG_USER_SIGNUP_SUCCESS = "Thank-you for registrations!  Now you can login."
MSG_FORGOT_PASSWORD_MAIL_SUCCESS = "We sent you an password recovery link " \
                                   "in you mail address."
MSG_CHANGE_PASSWORD_SUCCESS = "Your password has been updated successfully."