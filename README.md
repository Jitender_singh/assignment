### INTRODUCTION

This is a Djnago project uses the SQLAlachmy ORM and Postgres.

NOTE:- Please install the postgres into the system. And change the database settings in `testAssignment/settings.py` file
by changing the `DATABASES` variable  values.



### HOW TO INSTALL

Run following commands:-

> pip install -r requirement.txt

> python manage.py syncdb

> python manage.py loaddata fixtures/initial_data.json

> python manage.py runserver

After that insure that all data has been loaded.


Then open the link http://127.0.0.1:8000/ 
Login through following credentials


->username:- jittu
->password:- 123456


you will see the map with some map marker.that are connected with each other through lines.

you can generate the report according to date range.


## Admin site

Admin site link http://127.0.0.1:8000/admin/login/

you can login at admin site through following credentials

->username=admin
->password=123456

# Admin feature 

you can add and update locations, objects, users and UserObjects.
You can change the setting of the MAX_POINT and MAX_OBJECT by updating the MapSetting table.







