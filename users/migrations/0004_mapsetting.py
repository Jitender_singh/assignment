# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20151114_0616'),
    ]

    operations = [
        migrations.CreateModel(
            name='MapSetting',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('max_point', models.IntegerField(default=0)),
                ('max_object', models.IntegerField(default=0)),
            ],
        ),
    ]
