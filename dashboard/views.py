from django.shortcuts import HttpResponse
from django.views import generic
from users.models import ObjectUser, Location, MapSetting
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from users.core.utils import convert_to_date
import json
# Create your views here.


def return_location_data(request):
    # Return the location Points and details
    # handle ajax request
    return HttpResponse(json.dumps(ObjectUser.get_all_visible_objects(request.user.id)))


def get_report(request):
    # Generate the reports.
    # handle the ajax request.
    start_date = convert_to_date(request.GET.get('start_date', None))
    end_date = convert_to_date(request.GET.get('end_date', None))
    object_id = request.GET.get('object_id', None)
    with_limit = request.GET.get('point_limit', None)
    return HttpResponse(json.dumps(Location.get_report(object_id, start_date, end_date, with_limit)))


class DashboardView(generic.TemplateView):
    """
    View handle and show the dash-board
    """
    template_name = "dashboard/dashboard.html"

    @method_decorator(login_required(login_url=reverse_lazy('users:login')))
    def dispatch(self, request, *args, **kwargs):
        MapSetting.set_settings()  # set the settings
        return super(DashboardView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['points'] = json.dumps(ObjectUser.get_all_visible_objects(self.request.user.id))
        context['start_date'] = self.request.GET.get('start', None)
        context['end_date'] = self.request.GET.get('end', None)
        context['vehicle'] = self.request.GET.get('obj', None)
        objects_list = ObjectUser.get_users_object(self.request.user.id)

        context['objects_list'] = {obj.object.nid: obj.object.name for obj in objects_list}
        return context


class ReportView(generic.TemplateView):
    """
    View handle and show the report-section
    """
    template_name = "dashboard/report-section.html"

    @method_decorator(login_required(login_url=reverse_lazy('users:login')))
    def dispatch(self, request, *args, **kwargs):
        MapSetting.set_settings()  # set the settings
        return super(ReportView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(ReportView, self).get_context_data(**kwargs)
        objects_list = ObjectUser.get_users_object(self.request.user.id)
        context['objects_list'] = {obj.object.nid: obj.object.name for obj in objects_list}
        return context


class SettingView(generic.TemplateView):
    """
    View handle and show the report-section
    """
    template_name = "dashboard/setting-section.html"

    @method_decorator(login_required(login_url=reverse_lazy('users:login')))
    def dispatch(self, request, *args, **kwargs):
        MapSetting.set_settings()  # set the settings
        return super(SettingView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SettingView, self).get_context_data(**kwargs)
        objects_list = ObjectUser.get_users_object(self.request.user.id)
        context['objects_list'] = {obj.object.nid: obj.object.name for obj in objects_list}
        return context