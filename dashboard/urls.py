from django.conf.urls import url
from . import views

urlpatterns = [

    url(r'^$',
        views.DashboardView.as_view(),
        name='dashboard_page'),

    url(r'^report/$',
        views.ReportView.as_view(),
        name='report_page'),

    url(r'^setting/$',
        views.SettingView.as_view(),
        name='setting_page'),

    url(r'^get_location_data/',
        views.return_location_data,
        name='return_location_data'),

    url(r'^get_report/',
        views.get_report,
        name='get_report'),

]
