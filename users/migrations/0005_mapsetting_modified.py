# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_mapsetting'),
    ]

    operations = [
        migrations.AddField(
            model_name='mapsetting',
            name='modified',
            field=models.DateTimeField(default=datetime.datetime(2015, 11, 14, 14, 13, 16, 95797, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
    ]
